# Provider
provider "aws" {
  region = "us-east-1"
}

# Terraform Backend
terraform {
  backend "http" {
  }
}

# Persistent Volume
resource "aws_ebs_volume" "whoson_persistent_vol" {
  availability_zone    = "us-east-1a"
  encrypted            = false
  iops                 = 3000
  multi_attach_enabled = false
  size                 = 1
  tags = {
    "application " = "whoson"
  }
  tags_all = {
    "application " = "whoson"
  }
  throughput = 125
  type       = "gp3"
}

# Launch Template
resource "aws_launch_template" "whoson_tmplt" {
  update_default_version = true
  image_id               = "ami-080e1f13689e07408"
  key_name               = "whoson"
  name                   = "whoson-spot"
  tags = {
    application = "whoson"
  }
  tags_all = {
    application = "whoson"
  }
  user_data = base64encode(
    templatefile(
      "${path.module}/user-data.sh",
      {
        volume_id = aws_ebs_volume.whoson_persistent_vol.id
      }
    )
  )
  iam_instance_profile {
    arn = "arn:aws:iam::166252543379:instance-profile/EC2AttachVolume"
  }
  network_interfaces {
    subnet_id = "subnet-0b8e93e0fc14fbc61"
  }
  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      delete_on_termination = "true"
      encrypted             = "false"
      iops                  = 3000
      snapshot_id           = "snap-057fda6c9eb1d88ec"
      throughput            = 125
      volume_size           = 8
      volume_type           = "gp3"
    }
  }
}

# Spot Fleet Request
resource "aws_spot_fleet_request" "whoson_spot_request" {
  allocation_strategy                 = "lowestPrice"
  excess_capacity_termination_policy  = "Default"
  fleet_type                          = "maintain"
  iam_fleet_role                      = "arn:aws:iam::166252543379:role/aws-ec2-spot-fleet-tagging-role"
  instance_interruption_behaviour     = "terminate"
  load_balancers                      = []
  target_group_arns                   = []
  instance_pools_to_use_count         = 1
  on_demand_allocation_strategy       = "lowestPrice"
  on_demand_target_capacity           = 0
  replace_unhealthy_instances         = false
  tags                                = {}
  tags_all                            = {}
  target_capacity                     = 1
  terminate_instances_with_expiration = true
  valid_from                          = "2024-04-22T01:56:51Z"
  valid_until                         = "2025-04-22T01:56:51Z"
  depends_on                          = [aws_launch_template.whoson_tmplt]

  launch_template_config {
    launch_template_specification {
      id      = aws_launch_template.whoson_tmplt.id
      version = aws_launch_template.whoson_tmplt.latest_version
    }
    overrides {
      instance_type     = "t2.micro"
      priority          = 0
      subnet_id         = "subnet-0b8e93e0fc14fbc61"
      weighted_capacity = 1
    }
    overrides {
      instance_type     = "t3.micro"
      priority          = 0
      subnet_id         = "subnet-0b8e93e0fc14fbc61"
      weighted_capacity = 1
    }
    overrides {
      instance_type     = "t3.nano"
      priority          = 0
      subnet_id         = "subnet-0b8e93e0fc14fbc61"
      weighted_capacity = 1
    }
  }

  lifecycle {
    ignore_changes = [
      # Ignore changes due to autoscaling scheduled actions
      target_capacity
    ]
  }
}

resource "aws_appautoscaling_target" "whoson_spot_request" {
  max_capacity       = 1
  min_capacity       = 0
  resource_id        = "spot-fleet-request/${aws_spot_fleet_request.whoson_spot_request.id}"
  scalable_dimension = "ec2:spot-fleet-request:TargetCapacity"
  service_namespace  = "ec2"
  depends_on         = [aws_spot_fleet_request.whoson_spot_request]
  lifecycle {
    ignore_changes = [
      # Ignore changes due to autoscaling scheduled actions
      max_capacity, min_capacity
    ]
  }

}

resource "aws_appautoscaling_scheduled_action" "weekday_shutdown" {
  name               = "weekday shutdown"
  service_namespace  = "ec2"
  resource_id        = "spot-fleet-request/${aws_spot_fleet_request.whoson_spot_request.id}"
  scalable_dimension = "ec2:spot-fleet-request:TargetCapacity"
  schedule           = "cron(0 4 ? * MON-FRI *)"
  depends_on         = [aws_spot_fleet_request.whoson_spot_request, aws_appautoscaling_target.whoson_spot_request]

  scalable_target_action {
    max_capacity = 0
    min_capacity = 0
  }
}

resource "aws_appautoscaling_scheduled_action" "weekday_startup" {
  name               = "weekday startup"
  service_namespace  = "ec2"
  resource_id        = "spot-fleet-request/${aws_spot_fleet_request.whoson_spot_request.id}"
  scalable_dimension = "ec2:spot-fleet-request:TargetCapacity"
  schedule           = "cron(0 20 ? * MON-FRI *)"
  depends_on         = [aws_spot_fleet_request.whoson_spot_request, aws_appautoscaling_target.whoson_spot_request]

  scalable_target_action {
    max_capacity = 1
    min_capacity = 1
  }
}
