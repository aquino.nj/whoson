#!/bin/bash
### Set Vars
VOLUME_ID="$${volume_id:=vol-0b00de263f68072d4}"
POETRY_VERSION="1.3.2"

### Config Repos
add-apt-repository -y ppa:deadsnakes/ppa
apt update

### Setup AWS Resources
apt-get -y install awscli
OUTPUT=$(curl http://169.254.169.254/latest/meta-data/instance-id)
aws ec2 attach-volume --volume-id "$${VOLUME_ID}" --device /dev/xvdf --instance-id $OUTPUT --region us-east-1

### Setup DATA Dir
mkdir /data
NEXT_WAIT_TIME=0
until [ $NEXT_WAIT_TIME -eq 10 ] || mount /dev/xvdf /data || mount /dev/nvme1n1 /data; do
    sleep $(( NEXT_WAIT_TIME++ ))
done
chmod 777 /data
cd /data

### Setup rc.local to auto-mount on reboot
cat << 'EOF' > /etc/rc.local
#!/bin/sh
mount /dev/xvdf /data || mount /dev/nvme1n1 /data
EOF
chmod +x /etc/rc.local

### Install PreReqs
apt-get -y install python3.10 ffmpeg
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 1

### Setup everything as ubuntu
sudo -u ubuntu -i <<'EOF'

echo 'export POETRY_INSTALLER_PARALLEL=false' >> ~/.profile
echo 'export POETRY_HOME=/data/poetry' >> ~/.profile
echo 'export PATH="$POETRY_HOME/bin:$PATH"' >> ~/.profile
source ~/.profile
curl -sSL https://install.python-poetry.org | python3 - --version "$${POETRY_VERSION}"
poetry config cache-dir /data/cache/pypoetry

### Install Whoson
ssh-keyscan -t ecdsa gitlab.com >> ~/.ssh/known_hosts
url="https://gitlab.com/aquino.nj/whoson.git"
folder="whoson"
if ! git clone "$${url}" "$${folder}" 2>/dev/null && [ -d "$${folder}" ] ; then
    echo "Clone failed because the folder $${folder} exists"
fi
cd whoson
git fetch
git pull
poetry install

EOF

### Configure Whoson
cp /data/secrets/config.py /data/whoson/whoson/config.py
cp /data/whoson/whoson.service /etc/systemd/system/
systemctl enable whoson
systemctl start whoson

### Set up Gitlab Runner
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
gitlab-runner install --user=ubuntu --working-directory=/home/ubuntu
cp /data/gitlab-runner/config.toml /etc/gitlab-runner/config.toml
rm /home/ubuntu/.bash_logout
gitlab-runner start