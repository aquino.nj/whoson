from typing import List
from unittest.mock import MagicMock, AsyncMock

import pytest
from discord import Guild, Member, VoiceState, VoiceClient
from discord.ext.commands import Context
from pytest_mock import MockerFixture

from whoson import main


@pytest.mark.asyncio
async def test_on_ready(capfd, mocker):
    """Test on_ready function.

    Assert that the function logs in the expected user.
    """
    # Arrange
    test_user = "TestUser"
    mocker.patch.object(main, "bot", user=test_user)

    # Act
    await main.on_ready()
    out, err = capfd.readouterr()

    # Assert
    assert out == f"We have logged in as {test_user}\n"


@pytest.mark.asyncio
async def test_update_member_status(mock_guild: Guild, mock_member: Member):
    """Test update_member_status function.

    Assert that the function sends a message to the expected channel.
    """
    # Arrange
    voice_channel = mock_guild._channels["voice-channel"]
    member_status_channel = mock_guild._channels["member-status"]

    voice_state = MagicMock(spec=VoiceState, channel=voice_channel)

    # Act
    await main.update_member_status(mock_member, voice_state)

    # Assert
    member_status_channel.send.assert_called_once_with("TestUser is online")


@pytest.mark.asyncio
async def test_update_member_handles_no_channel(mock_guild: Guild, mock_member: Member):
    """Test that the function does not raise exception when member-status channel does not exist."""
    # Arrange
    voice_channel = mock_guild._channels["voice-channel"]
    del mock_guild._channels["member-status"]

    voice_state = MagicMock(spec=VoiceState, channel=voice_channel)

    # Act
    await main.update_member_status(mock_member, voice_state)

    # Assert
    # No exception should be raised


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "before_channel, after_channel, send",
    [
        (None, "voice-channel", True),
        ("voice-channel", "voice-channel", False),
        ("voice-channel", None, False),
        (None, None, False),
    ],
)
async def test_on_voice_state_update_handles_status_update(
        before_channel: str, after_channel: str, send: bool, mock_guild: Guild, mock_member: Member
):
    """Test on_voice_state_update function with various input parameters.

    Assert that the function sends a message to the expected channel, or not.
    """
    # Arrange
    member_status_channel = mock_guild._channels["member-status"]

    voice_state_before = MagicMock(spec=VoiceState, channel=mock_guild._channels.get(before_channel))
    voice_state_after = MagicMock(spec=VoiceState, channel=mock_guild._channels.get(after_channel))

    # Act
    await main.on_voice_state_update(mock_member, voice_state_before, voice_state_after)

    # Assert
    if send:
        member_status_channel.send.assert_called_once_with("TestUser is online")
    else:
        assert not member_status_channel.send.called


@pytest.mark.asyncio
async def test_on_voice_state_update_plays_saved_file(mocker: MockerFixture, mock_guild: Guild, mock_member: Member):
    """Test that the bot plays a saved file when a user joins a voice channel."""
    # Arrange
    mocker.patch("asyncio.sleep")
    mock_ffmpeg = mocker.patch.object(main, "FFmpegPCMAudio", return_value="mock_source")
    mocker.patch.object(main, "glob", return_value=["1.mp3"])

    voice_channel = mock_guild._channels.get("voice-channel")
    voice_client = MagicMock(spec=VoiceClient)
    # is_playing is called in this order
    #   1. Wait for current sound to finish playing
    #   2. Current sound is finished playing
    #   3. Restart play_next_sound(): Check if any other sound is playing before continuing
    voice_client.is_playing.side_effect = [True, False, False]
    # is_connected is called only once
    #   1. Check if voice_client is connected before disconnecting
    voice_client.is_connected.side_effect = [True]
    voice_channel.connect.return_value = voice_client
    voice_state_before = MagicMock(spec=VoiceState, channel=None)
    voice_state_after = MagicMock(spec=VoiceState, channel=voice_channel)

    # Act
    await main.on_voice_state_update(mock_member, voice_state_before, voice_state_after)

    # Assert
    mock_ffmpeg.assert_called_once_with("1.mp3")
    voice_client.play.assert_called_once_with("mock_source")
    voice_client.disconnect.assert_called_once()


@pytest.mark.asyncio
async def test_on_voice_state_update_returns_for_bot(mocker: MockerFixture, mock_guild: Guild, mock_member: Member):
    """Test that the bot returns if the bot itself updates its voice state."""
    # Arrange
    voice_state_before = MagicMock(spec=VoiceState, channel=MagicMock())
    voice_state_after = MagicMock(spec=VoiceState, channel=MagicMock())
    mocker.patch.object(main, "bot", user=mock_member)

    # Act
    await main.on_voice_state_update(mock_member, voice_state_before, voice_state_after)

    # Assert
    assert not voice_state_before.channel.assert_not_called()
    assert not voice_state_after.channel.assert_not_called()


class Contains(str):
    def __eq__(self, other):
        return self in other


@pytest.mark.asyncio
async def test_walk_on_cli_help():
    """Test that the bot responds with usage instructions when the user enters the 'help' command."""
    # Arrange
    mock_ctx = MagicMock(spec=Context)
    command = "help"

    # Act
    await main.walk_on_cli(mock_ctx, command)

    # Assert
    mock_ctx.send.assert_called_once_with(Contains("usage:"))


@pytest.mark.parametrize(
    'test_command',
    [
        ["set", "music"],
        ["set", "music", 1],
        ["set", "music", 1, 2]
    ])
@pytest.mark.asyncio
async def test_walk_on_cli_set(test_command: List[str], mocker: MockerFixture):
    """Test that the bot calls the 'set_walk_on' and 'get_walk_on' functions appropriately when the user enters the 'set' command."""
    # Arrange
    mock_set = mocker.patch.object(main, "set_walk_on", AsyncMock())
    mock_get = mocker.patch.object(main, "get_walk_on", AsyncMock())
    mock_ctx = MagicMock(spec=Context)
    commands = test_command

    # Act
    await main.walk_on_cli(mock_ctx, *commands)

    # Assert
    mock_set.assert_called_once_with(mock_ctx, *commands[1:])
    mock_get.assert_called_once()


@pytest.mark.asyncio
async def test_walk_on_cli_get(mocker):
    """Test the walk_on_cli function when called with the get command."""
    # Arrange
    mock_get = mocker.patch.object(main, "get_walk_on", AsyncMock())
    mock_ctx = MagicMock(spec=Context)
    commands = ["get"]

    # Act
    await main.walk_on_cli(mock_ctx, *commands)

    # Assert
    mock_get.assert_called_once_with(mock_ctx)


@pytest.mark.asyncio
async def test_walk_on_play(mocker):
    """Test the walk_on_cli function when called with the play command."""
    # Arrange
    mock_play = mocker.patch.object(main, "play_channel_sound", AsyncMock())
    mock_ctx = MagicMock(spec=Context)
    commands = ["play"]

    # Act
    await main.walk_on_cli(mock_ctx, *commands)

    # Assert
    mock_play.assert_called_once_with(mock_ctx.author, mock_ctx.author.voice.channel)


@pytest.mark.asyncio
async def test_walk_on_help():
    """Test the walk_on_cli function when called with the help command."""
    # Arrange
    mock_ctx = MagicMock(spec=Context)
    commands = ["help"]

    # Act
    await main.walk_on_cli(mock_ctx, *commands)

    # Assert
    mock_ctx.send.assert_called_once_with(Contains("usage:"))


@pytest.mark.asyncio
async def test_walk_on_cli_empty_command():
    """Test the walk_on_cli function when called with no command."""
    # Arrange
    mock_ctx = MagicMock(spec=Context)

    # Act
    await main.walk_on_cli(mock_ctx)

    # Assert
    mock_ctx.send.assert_called_once_with(Contains("usage:"))


@pytest.mark.parametrize(
    'test_args',
    [
        ["music"],
        ["anything"],
    ])
def test_download_music_file(test_args, mock_member, mocker):
    """Test the download_music function when called with a file to download."""
    # Arrange
    mock_download = mocker.patch.object(main, "download_site", MagicMock())

    # Act
    main.download_music(mock_member, *test_args)

    # Assert
    mock_download.assert_called_once_with(mock_member, test_args[0], 0, None)


@pytest.mark.parametrize(
    'test_args,expected_args',
    [
        (["music"], ["music", 0, None]),
        (["music", 1, 2], ["music", 1.0, 2.0]),
        (["music", '1:2', '1:3'], ["music", 62.0, 63.0])
    ])
def test_download_music_file_with_times(test_args, expected_args, mock_member, mocker):
    """Test the download_music function when called with a file to download."""
    # Arrange
    mock_download = mocker.patch.object(main, "download_site", MagicMock())

    # Act
    main.download_music(mock_member, *test_args)

    # Assert
    mock_download.assert_called_once_with(mock_member, expected_args[0], expected_args[1], expected_args[2])


@pytest.mark.parametrize(
    'test_args,test_vals',
    [
        (["youtube.com"], ['youtube', 0.0, None]),
        (["youtube.com", "1"], ['youtube', 1.0, None]),
        (["youtube.com", "1", "2"], ['youtube', 1.0, 2.0]),
        (["youtube.com", "1:20", "2:00"], ['youtube', 80.0, 120.0]),
        (["youtube.com", "1:20.5", "2:00.7"], ['youtube', 80.5, 120.7]),
    ])
def test_download_music_youtube(test_args, test_vals, mock_member, mocker):
    """Test the download_music function raises error when called with a YouTube URL to download."""
    # Act/Assert
    with pytest.raises(Exception, match=r"YouTube is no longer supported"):
        main.download_music(mock_member, *test_args)


@pytest.mark.parametrize(
    'test_args, start_time, end_time',
    [
        (["youtube"], 0, 10),
        (["youtube", 1], 1, 11),
        (["youtube", 1, 2], 1, 2),
        (["youtube", 1.3, 2], 1.3, 2)
    ])
def test_download_youtube(test_args, start_time, end_time, mock_member, mock_youtube_download, mock_audio_segment):
    """Test the download_youtube function."""
    # Act
    main.download_site(mock_member, *test_args)

    # Assert
    mock_youtube_download.assert_called_once()
    assert mock_youtube_download.call_args.args[0]['download_ranges']() == [
        {'start_time': start_time, 'end_time': end_time}]


def test_download_youtube_creates_audio_path(mock_member, mocker, mock_youtube_download, mock_makedirs,
                                             mock_audio_segment):
    """Test that download_youtube() creates the audio path if it doesn't exist."""
    # Arrange
    url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    mock_exists = mocker.patch("os.path.exists", return_value=False)

    # Act
    main.download_site(mock_member, url)

    # Assert
    mock_exists.assert_any_call(main.audio_path)
    mock_makedirs.assert_any_call(main.audio_path)


def test_download_youtube_creates_audio_file(mock_member, mocker, mock_youtube_download, mock_audio_segment):
    """Test that download_youtube() creates the audio file."""
    # Arrange
    url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    file_path = f"{main.audio_path}/{mock_member.id}.mp3"
    mock_exists = mocker.patch("os.path.exists", return_value=True)
    mock_remove = mocker.patch("os.remove")

    # Act
    main.download_site(mock_member, url)

    # Assert
    mock_exists.assert_any_call(file_path)
    mock_remove.assert_any_call(file_path)


def test_download_youtube_member_not_authorized_long_audio(mock_member, mock_youtube_download):
    """Test that download_youtube() raises an Exception when member is not authorized."""
    with pytest.raises(Exception):
        main.download_site(mock_member, "https://www.youtube.com/watch?v=dQw4w9WgXcQ", start_time=0,
                           end_time=100)  # assuming max_audio_length is less than 100


def test_download_youtube_member_is_authorized_long_audio(mock_youtube_download, mock_audio_segment):
    """Test that download_youtube() does not raise an Exception when member is authorized."""
    mock_member = MagicMock(spec=Member, display_name="KnickJ", id=1)
    main.download_site(mock_member, "https://www.youtube.com/watch?v=dQw4w9WgXcQ", start_time=0, end_time=100)


@pytest.mark.parametrize("input_str, expected_output", [
    ("3:45", 225.0),
    ("0:30", 30.0),
    ("10:00", 600.0),
    ("10:00.5", 600.5),
    ("120", 120.0),
    ("120.5", 120.5)
])
def test_get_seconds(input_str, expected_output):
    """Test that get_seconds() returns the correct number of seconds."""
    # Act
    result = main.get_seconds(input_str)

    # Assert
    assert result == expected_output


def test_download_site_creates_audio_path(mock_member, mocker, mock_youtube_download, mock_makedirs):
    """Test that download_youtube() creates the audio path if it doesn't exist."""
    # Arrange
    url = f"https://example.com/file.mp3"

    mock_requests = MagicMock()
    mock_requests.status_code = 200
    mock_open = mocker.mock_open()
    mock_audio_segment = MagicMock()

    mocker.patch("requests.get", return_value=mock_requests)
    mocker.patch("builtins.open", mock_open)
    mocker.patch("pydub.AudioSegment.from_file", return_value=mock_audio_segment)

    mock_exists = mocker.patch("os.path.exists", return_value=False)

    # Act
    main.download_site(mock_member, url)

    # Assert
    mock_exists.assert_any_call(main.audio_path)
    mock_makedirs.assert_any_call(main.audio_path)


def test_download_site_with_failed_download(mock_member, mocker):
    # Arrange
    url = "https://www.example.com/fail.mp3"
    mock_requests = MagicMock()
    mock_requests.status_code = 404

    mocker.patch("requests.get", return_value=mock_requests)

    # Act/Assert
    with pytest.raises(Exception, match=r"HTTP Error 404: Not Found"):
        main.download_site(mock_member, url)


@pytest.mark.asyncio
async def test_set_walk_on(mocker, mock_member):
    ctx = MagicMock(spec=Context)
    ctx.author = mock_member

    # Mock the download_music function to avoid making actual network requests
    mocker.patch("whoson.main.download_music")
    await main.set_walk_on(ctx, "https://www.youtube.com/watch?v=dQw4w9WgXcQ")

    # Check if the bot sent the expected message to the user
    ctx.send.assert_called_once_with(f":notes: Set sound for {mock_member.display_name} :notes:")


@pytest.mark.asyncio
async def test_set_walk_on_exception(mocker, mock_member):
    ctx = MagicMock(spec=Context)
    ctx.author = mock_member

    # Mock the download_music function to avoid making actual network requests
    mocker.patch("whoson.main.download_music", side_effect=Exception('Boom!'))
    await main.set_walk_on(ctx, "https://www.youtube.com/watch?v=dQw4w9WgXcQ")

    # Check if the bot sent the expected message to the user
    ctx.send.assert_called_once_with(f"Error while setting sound for {mock_member.display_name}.\n```css\n[Boom!]\n```")


@pytest.mark.asyncio
async def test_get_walk_on(mocker, mock_member):
    ctx = MagicMock(spec=Context)
    ctx.author = mock_member
    mock_glob = mocker.patch("whoson.main.glob", return_value=[f"{main.audio_path}/{mock_member.id}.mp3"])
    mock_file = mocker.patch("whoson.main.File", return_value="mock contents")

    await main.get_walk_on(ctx)

    # Check if the bot sent the expected message to the user
    ctx.send.assert_called_once_with(file="mock contents")
