# Whoson Bot

Whoson is a Discord bot that allows users to set and get their own custom "walk-on" sound. This is the sound that plays when a user joins a voice channel in a Discord server.

## Usage

The bot supports the following commands:

### /whoson set {url} [start_time] [end_time]

Set the whoson sound for the current user.

- `url` is a required argument and can be either a direct audio link or a video link. The bot will automatically trim the audio to 10 seconds.
- `start_time` is an optional argument, it sets the start time of the audio in seconds.
- `end_time` is an optional argument, it sets the end time of the audio in seconds, with a maximum of `start_time + 10`.

### /whoson get

Get current whoson sound and post to text channel

### /whoson play

Play current whoson sound in voice channel.

### /whoson help

Get usage information about the bot.

## Local Installation

To install Whoson bot you need to have Python, Poetry, and ffmpeg installed on your system.

Additional details can be derived from [user-data.sh](infra/user-data.sh), but they are not targeted for a local deployment.

### Install the Dependencies

This project uses python 3.10 and ffmpeg. 
If you are on ubuntu install these with the following command.

```bash
apt-get -y install python3.10 ffmpeg
```

Install Poetry 1.3.2 using the latest support method at https://python-poetry.org/.
Here is an example: 

```bash
curl -sSL https://install.python-poetry.org | python3 - --version "1.3.2"
```

### Install the Application

Install the dependencies with poetry:

```bash
poetry install
```

Copy the [config.py.example](whoson/config.py.example) file in the whoson directory of the project and add your Discord bot token to it:

```bash
cp whoson/config.py.example whoson/config.py
```

### Running the Bot

To run the bot, simply run the following command in the root directory of the project:

```bash
poetry run python whoson/main.py
```



## Deployment

The deployment process is managed using GitLab CI/CD. The [.gitlab-ci.yml](.gitlab-ci.yml) file defines the pipeline stages and jobs for deploying the bot.

### Deployment Steps

1. **Validate**: Run linting and type checking using `flake8`, `mypy`, and `black`.
2. **Test**: Run unit tests using `pytest`.
3. **Review**: Deploy a review environment for merge requests.
4. **Deploy**: Deploy the application to the production environment.

## Infrastructure

The infrastructure for the Whoson bot is managed using Terraform. The [main.tf](infra/main.tf) file defines the resources required for the bot.

### Key Resources

- **AWS EBS Volume**: Persistent storage for the bot.
- **AWS Launch Template**: Configuration for launching EC2 instances.
- **AWS Spot Fleet Request**: Request for spot instances to run the bot.
- **AWS App Auto Scaling**: Auto-scaling configuration for the spot fleet.

## Contributing

We welcome contributions to Whoson bot! If you have any suggestions or find any bugs, please open an issue or submit a pull request.
